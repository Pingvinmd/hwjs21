const container = document.createElement('div');
      container.style.display = 'flex';
      container.style.flexWrap = 'wrap';
      document.body.appendChild(container);

      const drawCircleBtn = document.getElementById('draw-circle-btn');
      drawCircleBtn.addEventListener('click', () => {
        const diameterInput = document.createElement('input');
        diameterInput.type = 'number';
        diameterInput.placeholder = 'Діаметр кола';
        diameterInput.min = 1;
        diameterInput.max = 100;
        diameterInput.required = true;
        diameterInput.style.margin = '10px';
        
        const drawBtn = document.createElement('button');
        drawBtn.textContent = 'Намалювати';
        drawBtn.style.fontSize = '16px';
        drawBtn.style.padding = '10px';
        drawBtn.style.margin = '10px';
        drawBtn.addEventListener('click', () => {
          container.innerHTML = '';
          const diameter = parseInt(diameterInput.value);
          for (let i = 0; i < 100; i++) {
            const circle = document.createElement('div');
            circle.classList.add('circle');
            circle.style.width = `${diameter}px`;
            circle.style.height = `${diameter}px`;
            circle.style.backgroundColor = getRandomColor();
            circle.addEventListener('click', () => {
              container.removeChild(circle);
            });
            container.appendChild(circle);
          }
        });
        
        container.appendChild(diameterInput);
        container.appendChild(drawBtn);
      });
      
      function getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
      }